(ns kivi.store)

(def db (atom {}))

(defn get
  [db]
  (fn [k]
    (clojure.core/get @db k)))




(defn put
  [db]
  (fn [k v]
    (swap! db
           (fn [old-value]
             (assoc old-value k v)))))
